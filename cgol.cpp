#include <iostream>
#include <unistd.h>
#include <argp.h>
#include "Grid.hpp"
#include "Cell.hpp"
#include "FileParser.hpp"

int main(int argc, char ** argv) {
    int generations = 100;

    //TODO replace with getopt, add more options (speed, display char, etc)
    if (argc < 2) {
        std::cerr << "Usage: ./cgol filename" << '\n';
        exit(0);
    }
    if (argc >= 3) {
        generations = atoi(argv[2]);
    }

    FileParser fp;
    Grid grid = fp.parseGrid(argv[1]);

    for (int i = 1; i <= generations; i++) {
        system("clear");
        std::cout << grid.toString() << i << '\n';
        grid.updateGrid();
        usleep(100000);
    }
}
