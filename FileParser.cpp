#include <iostream>
#include <fstream>
#include "FileParser.hpp"

//Gets the height (in lines) of a seed file.
int FileParser::fileHeight(std::ifstream& gridFile) {
    int numLines = 0;
    std::string line;

    while (std::getline(gridFile, line)) {
        numLines++;
    }
    return numLines;
}

//Gets the width (in characters) of a seed file.
int FileParser::fileWidth(std::ifstream& gridFile) {
    std::string line;
    std::getline(gridFile, line);
    int minWidth = line.length();

    while (std::getline(gridFile, line)) {
        minWidth = (line.length() < minWidth) ? line.length() : minWidth;
    }
    return minWidth;
}

//Sets the contents of a grid from a seed file.
void FileParser::setGrid(std::ifstream& gridFile, Grid * grid) {
    for (int i = 0; i < grid->getHeight(); i++) {
        std::string line;
        std::getline(gridFile, line);
        for (int j = 0; j < grid->getWidth(); j++) {
            if (line.at(j) != '.') {
                grid->setCell(j, i, true);
            }
        }
    }
}

//Creates a grid from a seed file.
Grid FileParser::parseGrid(std::string fileName) {
    std::ifstream gridFile;
    gridFile.open(fileName);
    int height = fileHeight(gridFile);
    gridFile.clear();
    gridFile.seekg(0);
    int width = fileWidth(gridFile);
    std::cout << width << " " << height << "\n";
    Grid g(width, height);
    gridFile.clear();
    gridFile.seekg(0);
    setGrid(gridFile, &g);
    gridFile.close();
    return g;
}
