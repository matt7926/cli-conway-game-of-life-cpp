cgol: Cell.o Grid.o cgol.o FileParser.o
	g++ -o cgol Cell.o Grid.o cgol.o FileParser.o
	rm *.o

Cell.o: Cell.cpp
	g++ -c Cell.cpp

Grid.o: Grid.cpp
	g++ -c Grid.cpp

FileParser.o: FileParser.cpp
	g++ -c FileParser.cpp

cgol.o: cgol.cpp
	g++ -c cgol.cpp

clean:
	rm *.o cgol
