#include "Cell.hpp"

//Sets a cell to be alive or dead.
void Cell::setLiving(bool isLiving) {
    alive = isLiving;
}

//Sets the cell's number of neighbors.
void Cell::setNumNeighbors(int newNumNeighbors) {
    numNeighbors = newNumNeighbors;
}

//Checks if a cell is alive
bool Cell::isAlive() {
    return alive;
}

//Returns the number of neighbors of a cell.
int Cell::getNumNeighbors() {
    return numNeighbors;
}

//Constructor with parameter for life.
Cell::Cell(bool isAlive) {
    setLiving(isAlive);
}

//Default constructor, sets the cell to be dead.
Cell::Cell() {
    setLiving(false);
}
