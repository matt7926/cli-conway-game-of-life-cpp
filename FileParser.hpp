#ifndef FILE_PARSER_HPP
#define FILE_PARSER_HPP

#include <iostream>
#include <fstream>
#include "Grid.hpp"

class FileParser {
private:
    int fileHeight(std::ifstream&);
    int fileWidth(std::ifstream&);
    void setGrid(std::ifstream&, Grid *);
public:
    Grid parseGrid(std::string);
};

#endif
