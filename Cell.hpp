#ifndef CELL_HPP
#define CELL_HPP

class Cell {
private:
    bool alive;
    int numNeighbors;

public:
    void setLiving(bool isLiving);
    void setNumNeighbors(int newNumNeighbors);
    bool isAlive();
    int getNumNeighbors();
    Cell(bool);
    Cell();
};

#endif
