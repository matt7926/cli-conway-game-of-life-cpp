#ifndef GRID_HPP
#define GRID_HPP

#include <string>
#include "Cell.hpp"

class Grid {
private:
    int width;
    int height;
    Cell ** gridArray;
    void initGrid(int, int);
    void calculateNeigbors(int, int);
    void calculateAllNeighbors();
public:
    void setWidth(int);
    void setHeight(int);
    void setCell(int, int, bool);
    void updateGrid();
    int getWidth();
    int getHeight();
    bool isCellAlive(int, int);
    std::string toString();
    Grid(int, int);
    ~Grid();
};

#endif
