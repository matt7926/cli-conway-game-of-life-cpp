#include <string>
#include "Grid.hpp"

//Initialized a grid of dead cells.
void Grid::initGrid(int newWidth, int newHeight) {
    setWidth(newWidth);
    setHeight(newHeight);
    gridArray = new Cell * [newHeight];
    for (int i = 0; i < newHeight; i++) {
        gridArray[i] = new Cell[newWidth];
    }
}

//Sets the width variable of the grid.
void Grid::setWidth(int newWidth) {
    width =  newWidth;
}

//Sets the height variable of the grid.
void Grid::setHeight(int newHeight) {
    height = newHeight;
}

//Sets the status of a cell at given x,y coordinates.
void Grid::setCell(int x, int y, bool isAlive) {
    gridArray[y][x] = Cell(isAlive);
}

//Calculates the number of neighbors for a cell at (x,y).
void Grid::calculateNeigbors(int x, int y) {
    int total = 0;
    for (int i = -1; i < 2; i ++) {
        for (int j = -1; j < 2; j++) {
            if (x + i >= 0 && x + i < width && y + j >= 0 && y + j < height && ! (i == j && j == 0)) {
                if (gridArray[y + j][x + i].isAlive()) {
                    total += 1;
                }
            }
        }
    }
    gridArray[y][x].setNumNeighbors(total);
}

//Calculates the number of neighbors for all cells.
void Grid::calculateAllNeighbors() {
    for (int  i = 0; i < getWidth(); i++) {
        for (int j = 0; j < getHeight(); j++) {
            calculateNeigbors(i, j);
        }
    }
}

//Updates each cell in the grid based on their number of neighbors
void Grid::updateGrid() {
    calculateAllNeighbors();
    for (int  i = 0; i < getHeight(); i++) {
        for (int j = 0; j < getWidth(); j++) {
            int neighbors = gridArray[i][j].getNumNeighbors();
            if (gridArray[i][j].isAlive()) {
                if (neighbors < 2 || neighbors > 3) {
                    gridArray[i][j].setLiving(false);
                }
            } else {
                if (neighbors == 3) {
                    gridArray[i][j].setLiving(true);
                }
            }
        }
    }
}

//Returns the width variable.
int Grid::getWidth() {
    return width;
}

//Returns the height variable.
int Grid::getHeight() {
    return height;
}

//Checks if the cell at (x,y) is alive.
bool Grid::isCellAlive(int x, int y) {
    return gridArray[y][x].isAlive();
}

//Returns the grid as a string.
std::string Grid::toString() {
    std::string gridString = "";
    gridString += std::string(getWidth() * 2 + 2, '-') + "\n";
    for (int  i = 0; i < getHeight(); i++) {
        gridString += "|";
        for (int j = 0; j < getWidth(); j++) {
             gridString += gridArray[i][j].isAlive() ? "O " : "  ";
        }
        gridString += "|\n";
    }
    gridString += std::string(getWidth() * 2 + 2, '-') + "\n";
    return gridString;
}

//Constructs a grid with the given dimensions.
Grid::Grid(int newWidth, int newHeight) {
    initGrid(newWidth, newHeight);
}

//Destructor.
Grid::~Grid() {
    for (int i = 0; i < getHeight(); i++) {
        delete[] gridArray[i];
    }
    delete[] gridArray;
}
